import { browser, by, element, promise, ElementFinder, ElementArrayFinder } from 'protractor';
import { Element } from '@angular/compiler';

export class HomePage {

    navigateToHome(): promise.Promise<any> {
        return browser.get('/home');
    }

    getPageBrandName(): promise.Promise<string> {
        return element(by.css('Take-it-or-Leave-it')).getText();
    }

    getNavBar(): ElementFinder {
        return element(by.tagName('search'));
    }

    getNavigateButton(): ElementFinder {
        return this.getNavBar().all(by.css('a')).get(1);
    }

    getAddItemNavigationButton(): ElementFinder {
        return element(by.css('.ion-icon'));
    }
}