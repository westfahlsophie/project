import { browser } from 'protractor';
import { HomePage } from './home-page.po';

describe(' Home Page', () => {
    const homePage = new HomePage();

    beforeEach(() => {
        homePage.navigateToHome();
    });

    it('Should have the page brand name', () => {
        expect(homePage.getPageBrandName()).toEqual('Take it or Leave it');
    });

    it('Should locate the nav bar', () => {
        expect(homePage.getNavBar()).toBeDefined();
    });

    it('Should find the additem navigation button', () => {
        expect(homePage.getAddItemNavigationButton().getText()).toContain('additem');
    });
});