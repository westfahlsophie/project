import { AppPage } from './app.po';
import { browser, by, element } from 'protractor';

describe('new App', () => {
    let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
    browser.get('/');
  });

  describe('home screen', () => {
    beforeEach(() => {
      page.navigateTo('/tabs');
    });

    it('should redirect to profile page when PROFILE is clicked', () => {
      const profileButton = element(by.id('profile'));
      profileButton.click();
      expect(browser.driver.getCurrentUrl()).toContain('/profile');
    });
  });
});
