import { AppPage } from './app.po';
<<<<<<< HEAD
import { homedir } from 'os';
=======
import { browser, by, element } from 'protractor';
>>>>>>> 38fe249d786d0facade9c0d62ab29e4829d4c86a

describe('new App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
    browser.get('/');
  });

  describe('default screen', () => {
    beforeEach(() => {
      page.navigateTo('/login');
    });
    it('should default to login page', () => {
      expect(page.getParagraphText()).toContain('SIGN IN');
    });

    it('should have the resigter button', () => {
      expect(element(by.id('reg')).getText()).toEqual('REGISTER');
    });

    it('register button should work',() => {
      expect(element(by.id('reg')).isPresent()).toBeTruthy('The register button should be there'); 
      element(by.id('reg')).click(); 
    });
  });
  });
