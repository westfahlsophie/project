// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebase:{
    apiKey: "AIzaSyBtzPzn3Ol9YFnFpGjrOLbe7EOSbQlMAc0",
    authDomain: "project-team-1.firebaseapp.com",
    databaseURL: "https://project-team-1.firebaseio.com",
    projectId: "project-team-1",
    storageBucket: "project-team-1.appspot.com",
    messagingSenderId: "497550712863",
    appId: "1:497550712863:web:db10599c4a7198377cb52a",
    measurementId: "G-NHBPJD5FL6"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
