import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  profilepic: any;

  showPassword = false;
  passwordToggleIcon = 'eye-outline';

  //sex: string;
  firstname: string;
  surname: string;
  email: string;
  phone: string;
  password: string;
  password2: string;

  constructor(private storage: Storage, private router: Router, public toastController: ToastController) {}

  ngOnInit() {
    //this.sex = 'male';
  }

  register(){

    if(this.password === this.password2){
      if(this.email.substring(this.email.length -17, this.email.length)=== '@students.oamk.fi' || this.email.substring(this.email.length -8, this.email.length)=== '@oamk.fi'){
        if(this.firstname !='' && this.surname !='' && this.email !='' && this.phone !='' && this.password.length >= 8){
        
            //register and navigate to Home page
            this.setStorage();
            this.router.navigate(['../tabs/home']);
            
        }else{
            this.openToast("Registration failed. Please fill out every field and check if your Password has 8 characters or more.");
        }
      }else{
        this.openToast("Please select your OAMK school email ")
      }
      
    }else{
      this.openToast("The passwords you entered do not match.")
    }
    
  }
  
  setStorage(){

    this.storage.set('firstname',this.firstname);
    this.storage.set('surname',this.surname);
    this.storage.set('email',this.email);
    this.storage.set('phone',"00"+ this.phone);
    //this.storage.set('sex',this.sex);
    this.storage.set('password',this.password);
    
    this.storage.get('firstname').then( (val) =>{
      console.log(val)
    })
  }

  // show and do not show the password
  togglePassword(): void{
    this.showPassword = !this.showPassword;
    //change icon 
    if(this.passwordToggleIcon == 'eye-outline'){
      this.passwordToggleIcon ='eye-off-outline';
    } else {
      this.passwordToggleIcon = 'eye-outline';
    }    

  }

  public async openToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 5000,
      buttons: [
        {
          text: 'Okay',
          role: 'cancel',
          handler: () => {

          }
        }
      ]
    });
    toast.present();
  }

}
