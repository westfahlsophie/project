import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    email: string;
    password: string;
    showPassword = false;
    passwordToggleIcon = 'eye-outline';

  constructor(private router: Router, public storage: Storage, public toastController: ToastController) { }

  ngOnInit() {
    this.storage.get('email').then( (val) =>{
      this.email = val;
      console.log("Email:" + val);
    })
    this.storage.get('password').then( (val) =>{
      console.log("Password:" + val);
    })
  }

  // show/ do not show the password
  togglePassword(): void{
    this.showPassword = !this.showPassword;
    //change icon 
    if(this.passwordToggleIcon == 'eye-outline'){
      this.passwordToggleIcon ='eye-off-outline';
    } else {
      this.passwordToggleIcon = 'eye-outline';
    }

  }
  
  signin(){

    this.storage.get('password').then( (val) =>{
      if(val ===''){
        this.openToast('You are not registered yet. Please go to "Register".');
      }else{
        if(this.password=== val){
          this.router.navigate(['../tabs/home']);
        }else{
          this.openToast("Email or Password wrong!");
        }
      }
    }); 
  }

  register(){
    this.storage.get('email').then( (val) =>{
      if(!val){
        this.router.navigate(['../register']);
      }else{
        this.openToast("You are already registered. Please Sign in.");
      }
    }); 
  }

  public async openToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2500,
      buttons: [
        {
          text: 'Okay',
          role: 'cancel',
          handler: () => {

          }
        }
      ]
    });
    toast.present();
  }

  clearS(){
    this.storage.clear();
    this.storage.get('firstname').then( (val) =>{
      console.log(val);
    })
  }

}
