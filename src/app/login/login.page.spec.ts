import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { By } from '@angular/platform-browser';

import { LoginPage } from './login.page';
import { DebugElement } from '@angular/core';

describe('LoginPage', () => {
  let component: LoginPage;
  let fixture: ComponentFixture<LoginPage>;
  let debugEl : DebugElement;
  let htmlEl : HTMLElement; 

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create login page', () => {
    expect(component).toBeTruthy();
  });

  afterEach(() => {
    component = null;
    fixture.destroy();
    debugEl = null;
    htmlEl = null; 
  })

  it('should display login and password field', () => {
    const item1: any = {
      user: [
        {
          'email': 'test@oamk.fi',
        }
      ]
    }
    const item2: any = {
      password: [
        {
          'password': 'Password12',
        }
      ]
    }
    const items = item1 + item2;
    const testLogin = items.login[0]
    fixture.detectChanges();
    debugEl = fixture.debugElement.query(By.css('ion-input'));
    expect(htmlEl.textContent).toContain(testLogin.user);
    expect(htmlEl.textContent).toContain(testLogin.password);
  })
});
