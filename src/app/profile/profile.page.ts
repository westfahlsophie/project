import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  profilepic: any;
  firstname: string;
  surname: string;
  phone: string;
  email: string;
  password: string;


  constructor(private storage: Storage, private router: Router) { }

  ngOnInit() {

    this.storage.get('firstname').then((val) => {
      this.firstname = val ;
    });
    this.storage.get('surname').then((val) => {
      this.surname = val ;
    });
    this.storage.get('phone').then((val) => {
      this.phone = val ;
    });
    this.storage.get('email').then((val) => {
      this.email = val ;
    });
    this.storage.get('password').then((val) => {
      this.password = val ;
    });
  }

  logout(){
    this.router.navigate(['']);
  }

}
