import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Item } from './item';


@Injectable({
  providedIn: 'root'
})

export class ItemService {

  private items : Observable<Item[]>;
  private itemCollection : AngularFirestoreCollection<Item>;
  

  constructor(db: AngularFirestore) {
    this.itemCollection = db.collection<Item>('items');
    this.items = this.itemCollection.snapshotChanges().pipe(
      
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getItems(): Observable<Item[]> {
    return this.items;
  }
 
  getItem(id: string): Observable<Item> {
    return this.itemCollection.doc<Item>(id).valueChanges().pipe(
      take(1),
      map(item => {
        item.id = id;
        return item;
      })
    );
  }
 
  addItem(item: Item): Promise<DocumentReference> {
    return this.itemCollection.add({...item}); //.add(item)
  }
 
  updateItem(item: Item): Promise<void> {
    return this.itemCollection.doc(item.id).update({ offer: item.offer, photo: item.photo, title: item.title, description: item.description, category: item.category, owner: item.owner, phone: item.phone, email: item.email, hidenumer: item.hidenumber});
  }

  deleteItem(id: string): Promise<void> {
    console.log("delete item " + id);
    return this.itemCollection.doc(id).delete();
  }


}


