export class Item {

  public  id?: string;
  offer: string;
  photo: any;
  title: string;
  description: string;
  category: string;
  owner: string;
  phone: string;
  email: string;
  hidenumber: boolean;
    

    /*constructor(newOffer: string, newPhoto: any, newTitle: string, newDescription: string, newCategory: string, newOwner: string, newEmail: string, newPhone: string), newHidenumber: boolean{
        this.offer = newOffer;
        this.photo = newPhoto;
        this.title = newTitle;
        this.description = newDescription;
        this.category = newCategory;
        this.owner = newOwner;
        this.phone = newPhone;
        this.email = newEmail;
    }*/
}
