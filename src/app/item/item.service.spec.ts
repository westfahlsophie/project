import { TestBed, async } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { ItemService } from './item.service';

describe('ItemService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
    ]
  }));

  it('should be created item service', () => {
    const service: ItemService = TestBed.get(ItemService);
    expect(service).toBeTruthy();
  });

  it('should get database content from AngularFireStore', async(() => {
    const service: ItemService = TestBed.get(ItemService);
    service.getItem('itemCollection').subscribe((data) => {
      const items: any = [
        data.category,
        data.description,
        data.email,
        data.hidenumber,
        data.id,
        data.offer,
        data.owner,
        data.phone,
        data.photo,
        data.title
      ]
      const result = items;
      console.log(items);
      expect(result).toContain(items);
      expect(Array.isArray(items)).toBeTruthy;
      expect(items.length).toBeGreaterThan(0);
      return result;
    });
  }));

  it('should contain content from AngularFireStore', async(() => {
    const service: ItemService = TestBed.get(ItemService);
    service
      .getItem('itemCollection').subscribe((data => {
        const items: any = [
          data.category,
          data.description,
          data.email,
          data.hidenumber,
          data.id,
          data.offer,
          data.owner,
          data.phone,
          data.photo,
          data.title
        ]
        const firstItem = items[0];
        expect(typeof firstItem).toEqual('category');
        expect(Object.keys(firstItem)).toContain('email');
        expect(Object.keys(firstItem)).toContain('owner');
        expect(Object.keys(firstItem)).toContain('title');
      }));
  }));
});
