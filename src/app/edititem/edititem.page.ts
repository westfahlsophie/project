import { Component, OnInit } from '@angular/core';
import { ItemService } from '../item/item.service';
import { Item } from '../item/item';
import { Router } from '@angular/router';
import { Firebase } from '@ionic-native/firebase/ngx';
import { Storage } from '@ionic/storage';
import { ToastController } from '@ionic/angular';
import { PickerController } from "@ionic/angular";
import { PickerOptions } from "@ionic/core";
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-edititem',
  templateUrl: './edititem.page.html',
  styleUrls: ['./edititem.page.scss'],
})
export class EdititemPage implements OnInit {

  item: Item ={
    id: '',
    offer: '',
    title: '',
    description: '',
    photo: '',
    category: '',
    owner: '',
    phone: '',
    email: '',
    hidenumber: null
  }

  constructor(private activatedRoute: ActivatedRoute, public itemService: ItemService, public storage: Storage, public toastController: ToastController, public router: Router, private pickerController: PickerController) { }

  ngOnInit() {
  }

  public async openToast() {

    if(this.item.hidenumber===false){
      const toast = await this.toastController.create({
          message: "If you hide your phone number, users can just contact you via email anymore. Usually people prefer your phone number for getting in contact.",
          duration: 5000,
          buttons: [
            {
            text: 'Okay',
            role: 'cancel',
            handler: () => {

            }
            }
          ]
        });
        toast.present();

      }  
  }

  public async openDelete() {
    const toast = await this.toastController.create({
      message: "Delete this item? Doing so will permanently delete the data and connot be undone.",
      duration: 9000,
      color: "light",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'Delete',
          handler: () => {
            this.itemService.deleteItem(this.item.id);
            this.router.navigate(['../tabs/myitems']);
          }
        }
      ]
    });
    toast.present();
  }

  ionViewWillEnter() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');

    if(id){
      this.itemService.getItem(id).subscribe(item => {
        this.item = item;
      })
    }
  }

  

  saveChanges(){
    this.itemService.updateItem(this.item);
    this.router.navigate(['../tabs/myitems'])
  }

}
