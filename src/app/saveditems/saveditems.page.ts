import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Item } from '../item/item';
import { ItemService } from '../item/item.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs';
import { ObjectService} from '../saveditems/object.service';
import { Object} from '../saveditems/object';


@Component({
  selector: 'app-saveditems',
  templateUrl: './saveditems.page.html',
  styleUrls: ['./saveditems.page.scss'],
})
export class SaveditemsPage implements OnInit {

  ids = Array<Object>();
  

  constructor(private activatedRoute: ActivatedRoute, public itemService: ItemService, public storage: Storage, public router: Router) { 
    this.ids;
  }


  ngOnInit() {
    this.storage.get('ids').then((val) => {
      this.ids = JSON.parse(val);
      if(!this.ids){
        this.ids = [];
      }
    });
  }

  //Reload Items 
  doRefresh(event) {
    this.storage.get('ids').then((val) => {
      this.ids = JSON.parse(val);
      if(!this.ids){
        this.ids = [];
      }
    });
  
    setTimeout(() => {
      event.target.complete();
    }, 1000);
  }

  navigate(id){
    try{
      this.router.navigate(['/itemdetail', id])
    }catch (ex){
      
    }
  }

  delete(index){
    this.ids.splice(index,1);
    this.storage.set('ids', JSON.stringify(this.ids));
  }
}
