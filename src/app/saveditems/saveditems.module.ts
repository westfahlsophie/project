import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SaveditemsPageRoutingModule } from './saveditems-routing.module';

import { SaveditemsPage } from './saveditems.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SaveditemsPageRoutingModule
  ],
  declarations: [SaveditemsPage]
})
export class SaveditemsPageModule {}
