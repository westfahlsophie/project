import { Injectable } from '@angular/core';
import { Object} from './object'

@Injectable({
  providedIn: 'root'
})
export class ObjectService {

  ids = Array<Object>();

  constructor() { }
}
