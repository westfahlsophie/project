import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SaveditemsPage } from './saveditems.page';

describe('SaveditemsPage', () => {
  let component: SaveditemsPage;
  let fixture: ComponentFixture<SaveditemsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveditemsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SaveditemsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
