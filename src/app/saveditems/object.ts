export class Object {
    id: string;
    title: string;
    photo: any;

    constructor(newId: string, newTitle: string, newPhoto: any){
        this.id = newId;
        this.title = newTitle;
        this.photo = newPhoto;
    }
}

