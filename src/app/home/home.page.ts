import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Item } from '../item/item';
import { ItemService } from '../item/item.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Observable, from } from 'rxjs';
import { PopoverController } from '@ionic/angular';
import { FilterPage } from "../filter/filter.page";

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  
  private items : Observable <Item[]>;
  owner: string;
  search: string;

  category: string;
  showownitems: boolean;


  constructor(
    private activatedRoute: ActivatedRoute, 
    private itemService: ItemService, 
    private router: Router, 
    private storage: Storage,
    private popover: PopoverController
  ) {}

  ngOnInit() {
    
    this.items = this.itemService.getItems();
    this.search ='';

    //Get owner
    this.storage.get('firstname').then( (val) =>{
      this.owner = val
    })  
    this.storage.get('surname').then( (val) =>{
      this.owner = this.owner + " " +val
    })
    
    //Filter 
    
    this.storage.get('showownitems').then( (val) =>{
      this.showownitems= val;
    }) 
    this.storage.get('category').then( (val) =>{
      this.category= val;
    })
  }

  //Reload Items 
  doRefresh(event) {
    
    this.items = this.itemService.getItems();

    this.storage.get('showownitems').then( (val) =>{
      this.showownitems= val;
    })
    this.storage.get('category').then( (val) =>{
      this.category= val;
    })
    
    setTimeout(() => {
      event.target.complete();
    }, 1000);
  }

  navigate(){
    this.router.navigate(['../additem']);
  }

  async createPopover(){
    
    this.popover.create({component: FilterPage, showBackdrop: false}).then ((popoverElement) => {
      popoverElement.present();
    });
  }

}


