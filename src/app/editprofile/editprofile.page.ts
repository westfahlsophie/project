import { Component, OnInit, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';



@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.page.html',
  styleUrls: ['./editprofile.page.scss'],
})

@NgModule({
    schemas: [ NO_ERRORS_SCHEMA ]
      })

export class EditprofilePage implements OnInit {

  showPassword = false;
  passwordToggleIcon = 'eye-outline';

  profilepic: any;
  //sex: any;
  firstname: string;
  surname: string;
  email: string;
  phone: number;
  password: string;


  constructor(private storage: Storage, private router: Router, public toastController: ToastController) {
  }

  ngOnInit() {
    
    this.storage.get('firstname').then((val) => {
      this.firstname = val ;
    });
    this.storage.get('surname').then((val) => {
      this.surname = val ;
    });
    this.storage.get('phone').then((val) => {
      this.phone = val ;
    });
    this.storage.get('email').then((val) => {
      this.email = val ;
    });
    this.storage.get('password').then((val) => {
      this.password = val ;
    });
  }

  // show and do not show the password
  togglePassword(): void{
    this.showPassword = !this.showPassword;
    //change icon 
    if(this.passwordToggleIcon == 'eye-outline'){
      this.passwordToggleIcon ='eye-off-outline';
    } else {
      this.passwordToggleIcon = 'eye-outline';
    }

  }

  save(){
    
    this.setStorage();
    this.router.navigate(['../tabs/home']);
  }

  setStorage(){
    this.storage.set('firstname',this.firstname);
    this.storage.set('surname',this.surname);
    this.storage.set('email',this.email);
    this.storage.set('phone',this.phone);
    //this.storage.set('sex',this.sex);
    this.storage.set('password',this.password);
  }

  public async pushNote(){
    const toast = await this.toastController.create({
      message: "Unfortunately you cannot change your name.",
      duration: 5000,
      position: 'top',
      buttons: [
        {
          text: 'Okay',
          role: 'cancel',
          handler: () => {

          }
        }
      ]
    });
    toast.present();
  }

}
