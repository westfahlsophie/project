import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      { path: 'home', loadChildren: () => import('../home/home.module').then( m => m.HomePageModule)},
      { path: 'myitems', loadChildren: () => import('../myitems/myitems.module').then( m => m.MyitemsPageModule)},
      { path: 'saveditems', loadChildren: () => import('../saveditems/saveditems.module').then( m => m.SaveditemsPageModule)},
      { path: 'profile', loadChildren: () => import('../profile/profile.module').then( m => m.ProfilePageModule)},
      { path: 'additem', loadChildren: () => import('../additem/additem.module').then( m => m.AdditemPageModule)},
      {path: 'editprofile', loadChildren: () => import('../editprofile/editprofile.module').then( m => m.EditprofilePageModule)
      },
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
