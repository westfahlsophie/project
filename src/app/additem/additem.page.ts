import { Component, OnInit } from '@angular/core';
import { ItemService } from '../item/item.service';
import { Item } from '../item/item';
import { Router } from '@angular/router';
import { Firebase } from '@ionic-native/firebase/ngx';
import { Storage } from '@ionic/storage';
import { ToastController } from '@ionic/angular';
import { PickerController } from "@ionic/angular";
import { PickerOptions } from "@ionic/core";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-additem',
  templateUrl: './additem.page.html',
  styleUrls: ['./additem.page.scss'],
})

export class AdditemPage implements OnInit {

  image: any;
  owner: string;
  phone: string;
  email: string;
  checked: boolean;

  item: Item = {
    offer: '',
    title: '',
    description: '',
    photo: '',
    category: '',
    owner: '',
    phone: '',
    email: '',
    hidenumber: null
  }

  constructor(private activatedRoute: ActivatedRoute, public itemService: ItemService, public storage: Storage, public toastController: ToastController, public router: Router, private pickerController: PickerController) { }

  ngOnInit() {
    this.storage.get('firstname').then((val) => {
      this.owner = val
    })
    this.storage.get('surname').then((val) => {
      this.owner = this.owner + ' ' + val
    })
    this.storage.get('phone').then((val) => {
      this.phone = val
    })
    this.storage.get('email').then((val) => {
      this.email = val
    })
    this.item.hidenumber = false;
    this.item.offer = "offer ";
  }

  ionViewWillEnter() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');

    if (id) {
      this.itemService.getItem(id).subscribe(item => {
        this.item = item;
      })
    }
  }

  //Toast if not every field is filled
  public async openToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 5000,
      buttons: [
        {
          text: 'Okay',
          role: 'cancel',
          handler: () => {

          }
        }
      ]
    });
    toast.present();
  }

  addItem() {

    this.item.owner = this.owner;
    this.item.phone = this.phone;
    this.item.email = this.email;

    if (this.item.photo == '') {
      this.item.photo = "../assets/noitem.png"
    }

    if (this.item.owner != '', this.item.title != '', this.item.description != '', this.item.category != '', this.item.offer != '') {

      this.itemService.addItem(this.item).then(() => {
        console.log(this.item);
        this.openToast('Item added');
        this.router.navigateByUrl('tabs/home');
      }, err => {
        this.openToast('There was a problem adding your item.');
      });
    } else {

      this.openToast("Please fill in every field.")
      //console.log (this.item.offer + this.item.photo+ this.item.title+ this.item.description+ this.item.category+ this.item.owner+ this.item.email+ this.item.phone)

      console.log(this.item);
    }


  }

  showInfo() {
    this.openToast("If you hide your phone number, users can just contact you via email anymore. Usually people prefer your phone number for getting in contact.")
  }

  addPhoto(n) {
    // this.router.navigateByUrl('tabs/home');
    let f = document.getElementById('file' + n);

    if (f !== null) {
      if (f.style.visibility == 'inherit') {
        f.style.visibility = "hidden";
      } else {
        f.style.visibility = "inherit";
      }
    }
  }


}
