import { Component, OnInit } from '@angular/core';
import { ItemService } from '../item/item.service';
import { Item } from '../item/item';
import { Router } from '@angular/router';
import { Firebase } from '@ionic-native/firebase/ngx';
import { Storage } from '@ionic/storage';
import { ToastController } from '@ionic/angular';
import { PickerController } from "@ionic/angular";
import { PickerOptions } from "@ionic/core";
import { ActivatedRoute } from '@angular/router';
import { ObjectService} from '../saveditems/object.service';
import { Object} from '../saveditems/object';


@Component({
  selector: 'app-itemdetail',
  templateUrl: './itemdetail.page.html',
  styleUrls: ['./itemdetail.page.scss'],
})
export class ItemdetailPage implements OnInit {

  itemname ="star-outline"
  ids = Array<Object>();

  item: Item ={
    id: '',
    offer: '',
    title: '',
    description: '',
    photo: '',
    category: '',
    owner: '',
    phone: '',
    email: '',
    hidenumber: null
  }

  constructor(private activatedRoute: ActivatedRoute, public itemService: ItemService, public storage: Storage, public toastController: ToastController, public router: Router, private pickerController: PickerController) { 
    this.ids;
    console.log(this.ids);

  }

  ngOnInit() {
    
    this.storage.get('ids').then((val) => {
      this.ids = JSON.parse(val);
      if(!this.ids){
        this.ids = [];
      }
    });  
  }
 
  ionViewWillEnter() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');

    if(id){
      this.itemService.getItem(id).subscribe(item => {
        this.item = item;
      })
    }
  }

  favour(){
    if(this.itemname==="star-outline"){
      this.toggleStar();
   
      let alreadysaved = false;
      this.ids.forEach(element => {
        if(element.id == this.item.id){
         alreadysaved = true;
       }
      });

      if(alreadysaved === false){
       this.ids.push(new Object(this.item.id, this.item.title, this.item.photo));
       this.storage.set('ids', JSON.stringify(this.ids));
      }else{
       console.log("Already saved")
     }
      
     console.log(this.ids);
    }else{
      console.log("Already saved")
    }

    
  }

  toggleStar(){
    
    //change icon 
    if(this.itemname == 'star-outline'){
      this.itemname ='star';
    } else {
      this.itemname = 'star-outline';
    }
  }
}