import { Component, OnInit, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { Item } from '../item/item';
import { ItemService } from '../item/item.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-myitems',
  templateUrl: './myitems.page.html',
  styleUrls: ['./myitems.page.scss'],
})

@NgModule({
  imports: [RouterLink],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
  })

export class MyitemsPage implements OnInit {

  owner: string;
  private items : Observable <Item[]>;
  

  constructor(private activatedRoute: ActivatedRoute, private itemService: ItemService, private router: Router, private storage: Storage) { }

  ngOnInit() {
    
    this.items = this.itemService.getItems();

    this.storage.get('firstname').then( (val) =>{
      this.owner = val
    })  
    this.storage.get('surname').then( (val) =>{
      this.owner = this.owner + " " +val
    })
    console.log(this.owner);
  }

  

}
