import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyitemsPageRoutingModule } from './myitems-routing.module';

import { MyitemsPage } from './myitems.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyitemsPageRoutingModule
  ],
  declarations: [MyitemsPage]
})
export class MyitemsPageModule {}
