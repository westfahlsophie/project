import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-filter',
  templateUrl: './filter.page.html',
  styleUrls: ['./filter.page.scss'],
})
export class FilterPage implements OnInit {

  category: string;
  showownitems: boolean;

  constructor(private popover: PopoverController, private storage: Storage) { }

  ngOnInit() {
    
    this.storage.get('category').then( (val) =>{
      this.category= val;
    })

    this.storage.get('showownitems').then( (val) =>{
      this.showownitems= val;
    })
    
  }

  closePopover(){
    
    this.storage.set('category',this.category);
    this.storage.set('showownitems',this.showownitems);

    this.popover.dismiss();
  }

  getShowownitems():boolean{
    return this.showownitems;
  }

}
